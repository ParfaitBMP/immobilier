<?php

namespace Immobilier\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierSiteBundle:Default:index.html.twig');
    }
}
