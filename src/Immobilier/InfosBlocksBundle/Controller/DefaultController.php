<?php

namespace Immobilier\InfosBlocksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierInfosBlocksBundle:Default:index.html.twig');
    }
}
