<?php

namespace Immobilier\CenterBlocksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierCenterBlocksBundle:Default:index.html.twig');
    }
}
