<?php

namespace Immobilier\SliderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierSliderBundle:Default:index.html.twig');
    }
}
