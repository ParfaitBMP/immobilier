<?php

namespace Immobilier\PartnersLinksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierPartnersLinksBundle:Default:index.html.twig');
    }
}
