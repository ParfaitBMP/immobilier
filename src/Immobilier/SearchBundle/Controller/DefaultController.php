<?php

namespace Immobilier\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierSearchBundle:Default:index.html.twig');
    }
}
