<?php

namespace Immobilier\MainmenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierMainmenuBundle:Default:index.html.twig');
    }
}
