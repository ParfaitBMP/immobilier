<?php

namespace Immobilier\HomeContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ImmobilierHomeContentBundle:Default:index.html.twig');
    }
}
